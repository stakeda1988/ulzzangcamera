//
//  ViewController.swift
//  NormalCamera
//
//  Created by SHOKI TAKEDA on 3/20/16.
//  Copyright © 2016 morningcamera.com. All rights reserved.
//

import UIKit
import Social
import Foundation
import CoreImage

extension String {
    
    /// String -> NSString に変換する
    func to_ns() -> NSString {
        return (self as NSString)
    }
    
    func substringFromIndex(index: Int) -> String {
        return to_ns().substringFromIndex(index)
    }
    
    func substringToIndex(index: Int) -> String {
        return to_ns().substringToIndex(index)
    }
    
    func substringWithRange(range: NSRange) -> String {
        return to_ns().substringWithRange(range)
    }
    
    var lastPathComponent: String {
        return to_ns().lastPathComponent
    }
    
    var pathExtension: String {
        return to_ns().pathExtension
    }
    
    var stringByDeletingLastPathComponent: String {
        return to_ns().stringByDeletingLastPathComponent
    }
    
    var stringByDeletingPathExtension: String {
        return to_ns().stringByDeletingPathExtension
    }
    
    var pathComponents: [String] {
        return to_ns().pathComponents
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func stringByAppendingPathComponent(path: String) -> String {
        return to_ns().stringByAppendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        return to_ns().stringByAppendingPathExtension(ext)
    }
    
}

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var backImageView : UIImageView!
    var shipImageView : UIImageView!
    var originalImageheight:CGFloat!
    var originalImagewidth:CGFloat!

    var upCameraBool:Bool = true
    var globalImgWidth:CGFloat = 0
    var globalImgHeight:CGFloat = 0
    var globalImg:UIImage?
    var globalRatio:Double = 0
    var postImg:UIImage?
    
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var topImgHeight: NSLayoutConstraint!
    @IBOutlet weak var topImgWidth: NSLayoutConstraint!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var cameraImgView: UIImageView!
    @IBAction func reserve(sender: UIButton) {
        UIGraphicsBeginImageContext(cameraImgView.bounds.size)
        cameraImgView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        UIImageWriteToSavedPhotosAlbum(UIGraphicsGetImageFromCurrentImageContext(), self, nil, nil)
        UIGraphicsEndImageContext()
        dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        twitterBtn.backgroundColor = UIColor.clearColor()
        twitterBtn.setImage(UIImage(named: "twitter"), forState: .Normal)
        twitterBtn.imageView!.contentMode = .ScaleAspectFit
        
        fbBtn.backgroundColor = UIColor.clearColor()
        fbBtn.setImage(UIImage(named: "facebook"), forState: .Normal)
        fbBtn.imageView!.contentMode = .ScaleAspectFit
        
        saveBtn.backgroundColor = UIColor.clearColor()
        saveBtn.setImage(UIImage(named: "save"), forState: .Normal)
        saveBtn.imageView!.contentMode = .ScaleAspectFit
        
        cameraBtn.backgroundColor = UIColor.clearColor()
        cameraBtn.setImage(UIImage(named: "camera"), forState: .Normal)
        cameraBtn.imageView!.contentMode = .ScaleAspectFit
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if upCameraBool == false && globalImg != nil {
            
//            cameraImgView.hidden = false
//            cameraImgView.frame = CGRectMake(0, 0, 30, 30)
//            cameraImgView.contentMode = .ScaleAspectFit
//            cameraImgView.image = globalImg
            
            let picName:String = "nkusakina.jpg"
            self.shipImageView = UIImageView(image:UIImage(named:picName))
            self.backImageView = UIImageView(image:UIImage(named:picName))
            let ratio = globalImg!.size.height / globalImg!.size.width
            self.shipImageView.frame = CGRectMake(0, self.view.frame.height/2 - self.view.frame.width * ratio/2, self.view.frame.width, self.view.frame.width * ratio)
            self.backImageView.frame = CGRectMake(0, self.view.frame.height/2 - self.view.frame.width * ratio/2, self.view.frame.width, self.view.frame.width * ratio)
            originalImageheight = self.shipImageView.frame.height
            originalImagewidth = self.shipImageView.frame.width
            self.view.addSubview(self.backImageView)
            self.view.addSubview(self.shipImageView)
            
            let myImage : UIImage = UIImage.ResizeÜIImage(globalImg!, width: self.view.frame.width, height: self.view.frame.height)
            let options : NSDictionary = NSDictionary(object: CIDetectorAccuracyHigh, forKey: CIDetectorAccuracy)
            let detector : CIDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options as! [String : AnyObject])
            let faces : NSArray = detector.featuresInImage(CIImage(image: myImage)!)
            var transform : CGAffineTransform = CGAffineTransformMakeScale(1, -1)
            transform = CGAffineTransformTranslate(transform, 0, -self.view.frame.height)
            var globalLeftEyeX:Int = 0
            var globalLeftEyeY:Int = 0
            var globalFaceMinX:Int = 0
            var globalFaceMaxX:Int = 0
            var globalFaceMinY:Int = 0
            var globalFaceMaxY:Int = 0
            var faceCounter:Int = 0
            for feature in faces {
                if faceCounter == 0 {
                    globalLeftEyeX = Int(feature.leftEyePosition.x)
                    globalLeftEyeY = Int(feature.leftEyePosition.y)
                    globalFaceMinX = Int(feature.bounds.minX)
                    globalFaceMaxX = Int(feature.bounds.maxX)
                    globalFaceMinY = Int(feature.bounds.minY)
                    globalFaceMaxY = Int(feature.bounds.maxY)
                }
                faceCounter++
            }
            self.shipImageView?.image = globalImg
            let filteredImage = self.shipImageView?.image!.getFilteredImage(globalLeftEyeX, leftEyeY: globalLeftEyeY, faceMinX: globalFaceMinX, faceMaxX: globalFaceMaxX, faceMinY: globalFaceMinY, faceMaxY: globalFaceMaxY, imageHeight: originalImageheight, imageWidth: originalImagewidth, viewHeight: self.view.frame.height, viewWidth: self.view.frame.width)
            let ciContext = CIContext(options: nil)
            let filter = CIFilter(name: "CIPhotoEffectTransfer")
            let ciImage:CIImage = CIImage(image:filteredImage!)!
            filter!.setValue(ciImage, forKey: kCIInputImageKey)
            let filteredImageData = filter!.valueForKey(kCIOutputImageKey) as! CIImage
            let filteredImageRef = ciContext.createCGImage(filteredImageData, fromRect: filteredImageData.extent)
            let newFilteredImage = UIImage(CGImage: filteredImageRef)
            self.shipImageView?.image = newFilteredImage

            postImg = self.shipImageView?.image
        }
        cameraImgView.hidden = false
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if upCameraBool == true {
            let picker:UIImagePickerController = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(picker, animated: true, completion: nil)
        }
        upCameraBool = false
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        let stampViews = cameraImgView.subviews
        for stampView in stampViews {
            stampView.removeFromSuperview()
        }
        globalRatio = Double(image.size.height / image.size.width)
        globalImg = image
        topImgWidth.constant = self.view.frame.width
        topImgHeight.constant = self.view.frame.width * CGFloat(globalRatio)
        
        cameraImgView.hidden = false
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func twitter(sender: AnyObject) {
        let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        let title: String = "#UlzzangCamera"
        controller.setInitialText(title)
        controller.addImage(postImg)
        presentViewController(controller, animated: true, completion: {})
    }
    
    @IBAction func facebook(sender: AnyObject) {
        let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        let title: String = "#UlzzangCamera"
        controller.setInitialText(title)
        controller.addImage(postImg)
        presentViewController(controller, animated: true, completion: {})
    }
    
    @IBAction func instagram(sender: AnyObject) {
        let imageData = UIImageJPEGRepresentation(postImg!, 1.0)
        let temporaryDirectory = NSTemporaryDirectory() as String
        let temporaryImagePath = temporaryDirectory.stringByAppendingPathComponent("postImg.igo")
        let boolValue = imageData!.writeToFile(temporaryImagePath, atomically: true)
        var documentInteractionController = UIDocumentInteractionController()
        //        documentInteractionController.URL = temporaryImagePath
        documentInteractionController.UTI = "com.instagram.exclusivegram"
        //        documentInteractionController.presentOpenInMenuFromRect(
        //            postImg.bounds,
        //            inView: postImg,
        //            animated: true
        //        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

